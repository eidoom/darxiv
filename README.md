# [darxiv](https://gitlab.com/eidoom/darxiv)

[Live](https://eidoom.gitlab.io/darxiv)

Used [sveltejs/template](https://github.com/sveltejs/template).

## Dev

```bash
npm i
npm run dev
```

## Build

```bash
npm i
npm run build
# serve public/
```

## Notes

The undocumented `submittedDate` search option is used to call the arXiv API.
It uses the UTC time zone.

## Acknowledgements

[Thank you to arXiv for use of its open access interoperability.](https://arxiv.org/help/api/index)
