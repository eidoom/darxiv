import { spawn } from "child_process";
import svelte from "rollup-plugin-svelte";
import resolve from "@rollup/plugin-node-resolve";
import livereload from "rollup-plugin-livereload";
import terser from "@rollup/plugin-terser";
import css from "rollup-plugin-css-only";
import json from "@rollup/plugin-json";
import copy from "rollup-plugin-copy";

const production = !process.env.ROLLUP_WATCH;

function serve() {
  let server;

  function toExit() {
    if (server) server.kill(0);
  }

  return {
    writeBundle() {
      if (server) return;
      server = spawn("npm", ["run", "start", "--", "--dev"], {
        stdio: ["ignore", "inherit", "inherit"],
        shell: true,
      });

      process.on("SIGTERM", toExit);
      process.on("exit", toExit);
    },
  };
}

export default {
  input: "src/main.js",
  output: {
    sourcemap: false,
    format: "es",
    name: "app",
    file: "public/bundle.js",
  },
  plugins: [
    copy({
      targets: [
        { src: "static/*", dest: "public/" },
        { src: "node_modules/katex/dist/fonts/*", dest: "public/fonts" },
      ],
    }),

    svelte({
      compilerOptions: {
        // enable run-time checks when not in production
        dev: !production,
      },
    }),

    json(),

    // we'll extract any component CSS out into
    // a separate file - better for performance
    css({
      output: "bundle.css",
    }),

    // If you have external dependencies installed from
    // npm, you'll most likely need these plugins.
    resolve({
      browser: true,
      dedupe: ["svelte"],
    }),

    // In dev mode, call `npm run start` once
    // the bundle has been generated
    !production && serve(),

    // Watch the `public` directory and refresh the
    // browser on changes when not in production
    !production && livereload("public"),

    // If we're building for production (npm run build
    // instead of npm run dev), minify
    production && terser(),
  ],
  watch: {
    clearScreen: false,
  },
};
